import random
from sismic.io import import_from_yaml
from sismic.interpreter import Interpreter
from sismic.helpers import log_trace
# Load statechart from yaml file
machine = import_from_yaml(filepath='yaml/vendingmachine.yaml')

# Create an interpreter for this statechart
interpreter = Interpreter(machine)
#interpreter.execute()
#print('execution without event:', interpreter.configuration)
#print("coin before cCoin:", interpreter.context['nCoins'])
#interpreter.queue('cCoin')
#interpreter.execute()
#print("coin after cCoin:", interpreter.context['nCoins'])


events = ["cCoin","cCup","cTea","cCoffee"]
trace = log_trace(interpreter)
condition = True


def notry(choice):
    interpreter.execute()
def withtry(choice):
    taille = len(trace)
    global condition
    try:
        interpreter.execute()
    except:
        condition = False
        print("Exception levé")
        print("====================")
        print(interpreter.configuration)
        print(interpreter.context['nCoins'])
        print("====================")
        for element in trace:
            #print(element) 
            #print(element.event)
            #print(element.transitions)
            #print(element.entered_states)
            #print(element.exited_states)
            #print(element.steps)
            print("===============")
            print("évènement: ", element.event, "\ntransitions: " , element.transitions , "\nétats entrant: ", element.entered_states)
        print(trace[-1])
        print(trace[-1].steps)
while(condition):
    choice = random.choice(events)
    interpreter.queue(choice)
    withtry(choice)



condition = False

exist = False

while(condition and not(exist)):
    choice = random.choice(events)
    interpreter.queue(choice)
    withtry(choice)
    if (interpreter.context['rest'] >=3 and (('CoffeeRequest' in interpreter.configuration) or ('TeaRequest' in interpreter.configuration))):
        exist = True
        print("Condition vérifiée")
