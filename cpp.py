#!/usr/bin/env python3
import sys

from sismic.io import import_from_yaml
from sismic.model import (
    DeepHistoryState, FinalState, Transition, CompoundState,
    OrthogonalState, ShallowHistoryState, Statechart,
    ActionStateMixin, ContractMixin)



class CppExporter:
    def __init__(
            self,
            statechart: Statechart) -> None:
        self.statechart = statechart
        self._output = []  # type: List[str]
        self._indent = 0
        self.variable = set()
        self.transitions_cpt = {}
        
    def indent(self):
        self._indent += 2

    def deindent(self):
        self._indent -= 2

    def output(self, text: str, *, wrap: str='') -> None:
        lines = text.strip().split('\n')

        for line in lines:
            # Special case for __old__
            line = line.replace('__old__', '~__old__')

            self._output.append(
                '{indent}{wrap}{line}{wrap}'.format(
                    indent=' ' * self._indent,
                    wrap=wrap,
                    line=line
                )
            )

    def export_transitions(self) -> None:
        """
        Export all transitions of the statechart into self._output in a valid cpp code

        """
        for transition in self.statechart.transitions:        
            self.export_transition(transition)
                
    def export_transition(self,transition: Transition) -> None:
        """
        Export a given transition into self._output in a valid cpp code
        :param transition: the transition to export

        """
        transition_name = "{}_{}".format(transition.source,transition.source if transition.internal else transition.target)
        if transition_name not in self.transitions_cpt:
            self.transitions_cpt[transition_name]=0
        else:
            self.transitions_cpt[transition_name]+=1
        transition_name = transition_name if self.transitions_cpt[transition_name] == 0  else transition_name+"_"+str(self.transitions_cpt[transition_name])

        
        if not transition.guard and not transition.action:
            self.output("Transition {}({},{}{});".format(
            transition_name,
            transition.source,
            transition.source if transition.internal else transition.target,
            ","+transition.event if not transition.eventless else ""
            ))
            return

        
        captured = '['+','.join(["&"+variable for variable in self.variable])+']'
        lambdafunc = [captured]
        if transition.eventless:
            lambdafunc.append('(Evt_t evt) { return ')
        else:
            lambdafunc.append('(Evt_t evt) {{ return (evt == {}) '.format(transition.event))
        
        if transition.guard:
            lambdafunc[0] = "["+','.join(["&"+variable for variable in self.variable if variable in transition.guard])+"]"
            if not transition.eventless:
                lambdafunc.append('and ')
            lambdafunc.append('{}'.format(';'.join(filter(None,self.retrieve_cpp_code(transition.guard)))))
        lambdafunc.append(';}')

        self.output('Transition {}({},{},{},{});'.format(
            transition_name,
            transition.source,
            transition.source if transition.internal else transition.target,
            ''.join(lambdafunc),
            "empty_action" if transition.action is None else captured+"() {"+';'.join(filter(lambda e: e,self.retrieve_cpp_code(transition.action)))+";}",
        ))



    def export_states(self) -> None:
        """
        Export all the states of the statechart into self._output in valid cpp code

        """
        children = []
        history = set()
        initial = []
        for state in self.statechart.states:
            self.export_state(state)
            parent = self.statechart.parent_for(state)
            if (parent):
                children.append('{}.add_{}({});'.format(parent,"region" if isinstance(self.statechart.state_for(parent),OrthogonalState) else "child",state)) # for esthetic purpose, we print all the 'add_child' lines after the declaration of all states

            real_state = self.statechart.state_for(state)
            if (isinstance(real_state,CompoundState)):
                if(real_state.initial):
                    initial.append((state,real_state.initial))
            if isinstance(real_state, ShallowHistoryState):
                history.add('{}.set_history(true);'.format(parent))

        self.output('')

        for child in children:
            self.output(child)
        self.output('')
        for initstate in initial:
            self.output('{}.set_initial({});'.format(initstate[0],initstate[1]))

        self.output('')
        for history_state in history:
            self.output(history_state)
    def export_state(self, source_name: str) -> None:
        """
        Export a given state into self._output in valud cpp code
        :param source_name: the name of the state to output
        
        """
        state = self.statechart.state_for(source_name)
        if (isinstance(state,CompoundState)):
            str='CompositeState '

        elif (isinstance(state,OrthogonalState)):
            str='OrthogonalState '
        else:
            str='SimpleState '


        on_something = "(\""+source_name+"\")"
        if((state.on_entry and '#' in state.on_entry) or (state.on_exit and '#' in state.on_exit)):
            on_something = ['(','"'+source_name+'",',"empty_action",' , ',"empty_action",')']
            cpp_codes = list(filter(None , self.retrieve_cpp_code(state.on_entry)))
            if(cpp_codes != []):
                on_something[2] = ['[]','() {','','}']
                on_something[2][0] = "["+','.join(["&"+variable for variable in self.variable if any(variable in cpp_line for cpp_line in cpp_codes)])+"]"
                on_something[2][2] = ';'.join(cpp_codes)+';'
                on_something[2] = ''.join(on_something[2])

            cpp_codes = list(filter(None , self.retrieve_cpp_code(state.on_exit)))
            if(cpp_codes != []):
                on_something[4] = ['[]','() {','','}']
                on_something[4][0] = "["+','.join(["&"+variable for variable in self.variable if any(variable in cpp_line for cpp_line in cpp_codes)])+"]"
                on_something[4][2] = ';'.join(cpp_codes)+';'
                on_something[4] = ''.join(on_something[4])

            on_something = ''.join(on_something)
        self.output(str + source_name + on_something + ';')

    def export(self) -> str:
        """
        return the exported statechart in a valid cpp code
        :return: the cpp format of the statechart
        """
        self.retrieve_variable()
        self.export_include()
        self.output('')
        self.export_main_sm()
        self.output('')
        self.export_function()
        self.output('')
        self.export_main_func()
        return '\n'.join(self._output)


    def retrieve_variable(self) -> None:
        """
        retrieve all the variable in the preamble into self.variable
        """
        for variable in self.retrieve_cpp_code(self.statechart.preamble): # line of code with a # at first index
            if (variable and variable != '' and '=' in variable):
                self.variable.add(variable.split(' ')[1])
        
    
    def export_main_func(self) -> None:
        """
        Export the main cpp function into self._output
        """
        self.output('int main() {')
        self.indent()
        self.output('timer_list_init();')
        self.output('')

        for variable in self.retrieve_cpp_code(self.statechart.preamble): # line of code with a # at first index
            if variable:
                self.output(variable)
                self.output('')
        
        self.export_states()
        self.output('')
        
        self.export_transitions()  # type: ignore
        self.output('')
        
        self.output("MainSM sm("+ self.statechart.root +");")
        self.output('')
        
        self.output('int c = 0;')
        self.output('show('+",".join(self.variable)+');')
        self.output('while ( (c = getchar()) != \'q\') {')
        self.indent()
        self.output('show('+",".join(self.variable)+');')
        self.deindent()
        self.output('}')
        self.output('return 0;')
        self.deindent()
        self.output('}')
    
    def export_include(self) -> None:
        """
        Export the cpp includes and events into self._output
        """
        self.output('#include <sm_defs.h>')
        self.output('#include <timeout.h>')
        self.output('#include <state_chart.hpp>')
        self.output('#include <transition.hpp>')
        self.output('#include <iostream>')
        self.output('#include <pthread.h>')
        self.output('using namespace std;')
        self.output('enum Events {' + ' , '.join(self.statechart.events_for()) + '};')
    
    def export_main_sm(self) -> None:
        """
        Export the cpp class MainSM into self._output 
        """
        self.output('class MainSM : public StateMachine {')
        self.indent()
        self.output('SimpleState *root;')
        self.deindent()
        self.output('public:')
        self.indent()
        self.output('MainSM(SimpleState &s) : root(&s)')
        self.indent()
        self.output('{}')
        self.deindent()
        self.output('void handler(Evt_t evt) {')
        self.output('}')
        self.deindent()
        self.output('};')
        self.output('')


    def export_function(self) -> None:
        """
        Export all the functions used into self._output in a valid cpp code
        """
        def generic_output(element) -> None:
            if(element and '(' in element and element not in generic_output.allreadyin): # if element is a function we never met
                sub = element.split('(')
                substring = sub[1].strip()
                if substring[0] != ')': # there is at lest 1 argument
                    size = substring.count(',') + 1
                    element = sub[0]+'(' + ','.join(['int arg'+str(i) for i in range(size)])+')'
                else:
                    element = sub[0]+'()' # in order to not get what is after the parenthesis 
                if element not in generic_output.allreadyin:
                    self.output('void ' + element)
                    self.output('{')
                    self.output('}')
                    generic_output.allreadyin.add(element)
        generic_output.allreadyin = set()

        def generic_output_with_commentary(elements) -> None:
            for cpp_transition in self.retrieve_cpp_code(elements): # any line of code with a #
                generic_output(cpp_transition)
            
        for transition in map(lambda transition: transition.action ,self.statechart.transitions): # function in transtition
            generic_output_with_commentary(transition)
        
        for state in map(lambda namestate: self.statechart.state_for(namestate), self.statechart.states): # function in on_entry,on_exit
            generic_output_with_commentary(state.on_entry)
            generic_output_with_commentary(state.on_exit)

        generic_output("show("+",".join(self.variable)+")")

    def retrieve_cpp_code(self,codes:str) :
        """
        retrieve the cpp code wich is in commentary
        :return: the cpp code

        """
        return map(lambda line_of_code: line_of_code[1:] if line_of_code and line_of_code[0] == '#' else None,codes.split('\n') if codes else [])

def export_to_cpp(
        statechart: Statechart,
        filepath: str=None,
        ) -> str:
    """
    Export given statechart to cpp.
    If a filepath is provided, also save the output to this file.

    Due to the way statecharts are representing, and due to the presence of features that are specific to Sismic,
    the resulting cpp file does not include all the informations.
    For example, final states and history states won't have name, actions and contracts.

    :param statechart: statechart to export
    :param filepath: save output to given filepath, if provided
    :return: cpp
    """

    exporter = CppExporter(
        statechart
    )

    output = exporter.export()

    if filepath:
        with open(filepath, 'w') as f:
            f.write(output)

    return output


def main(yaml: str,file2copy:str) -> None:
    statechart = import_from_yaml(filepath=yaml)
    output = export_to_cpp(statechart,file2copy)
    if (not file2copy):
        print(output)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        main(sys.argv[1],None)
    elif len(sys.argv) == 3:
        main(sys.argv[1],sys.argv[2])
    else:
        print("Usage error : expected 1 or 2 argument, got {}".format(len(sys.argv)-1))
